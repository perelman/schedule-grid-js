# schedule-grid-js
Create and view schedule grids. Automatically organizes multiple
concurrent events into the minimum number of columns. Also supports
displaying a schedule for each individual person.

Schedules are stored only on the client's computer. No information is
sent to the server.

Hosted at https://aweirdimagination.net/apps/schedule-grid/
