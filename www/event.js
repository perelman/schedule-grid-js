import * as utils from './utils.js';

export default class Event {
  constructor(args) {
    this.assignment = args.assignment;
    this.start_time = args.start_time;
    this.end_time = args.end_time;
    this.width = args.width;
    this.track = args.track;
    this.description = args.description ?? "ERROR: MISSING DESCRIPTION";
    this.classes = args.classes ?? [];
    this.title = args.title;
    this.day = args.day;
  }

  clone() {
    return new Event({...this, description: this.description.cloneNode(true)});
  }

  get tracks() {
    if (this.track === 'all') return [];
    if (Array.isArray(this.track)) return this.track;
    // TODO What if track is undefined? This probably isn't called then?
    return [this.track];
  }

  get timeRange() {
    return new utils.TimeRange(this);
  }

  at_same_time_as(other) {
    return this.timeRange.overlaps(other.timeRange);
  }
}
